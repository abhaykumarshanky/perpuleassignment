import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HelloComponent } from './hello.component';
import { HttpClientModule } from '@angular/common/http';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { MoviecardComponent } from '../components/moviecard/moviecard.component';



@NgModule({
  imports:      [ BrowserModule, FormsModule,HttpClientModule, FilterPipeModule ],
  declarations: [ AppComponent, HelloComponent, MoviecardComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
