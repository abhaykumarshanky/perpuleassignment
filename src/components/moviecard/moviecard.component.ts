import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'my-moviecard-app',
  templateUrl: './moviecard.component.html',
  styleUrls: ['./moviecard.component.css']
})
export class MoviecardComponent {
    private result:number=1;
    private baseURL:string="http://www.omdbapi.com/";
    private apiKEY:string="3ee8eff8";
    private url;
    searchText: any = { Title: 'search' };
    noData:boolean=false;
    Response:any;
    constructor(private http: HttpClient) {}
    httpData:any;
    searchParameter:any;

    // API REQUEST FOR FETCHING THE MOVIE
    getData(e){
        if(e && e.target.value=="prev") this.result = this.result-1;
        else if(e && e.target.value=="next") this.result = this.result+1;
        else this.result=1;
        // this.url=`${this.baseURL}?${this.dropoption==='Title'?("t="+(this.searchText).Title) :("s="+(this.searchText).Title)}&page=${this.result}&apikey=${this.apiKEY}`;
        this.url=`${this.baseURL}?${"s="+(this.searchText).Title}&page=${this.result}&apikey=${this.apiKEY}`;
        this.http.get<Kafein[]>(this.url).subscribe(data => {
            this.httpData=data;
            this.Response=JSON.parse((this.httpData.Response).toLowerCase());
            if(!this.Response) this.noData=true
            else{
                this.noData=false
                this.searchParameter=this.httpData.Search;
            }
        })
    }
    // ON INIT CALLING GETDATA API
    ngOnInit() {
        this.getData(null);    
    }
  

}
interface Kafein {
  Title:string;
  Poster:string;
}